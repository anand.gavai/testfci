FROM ubuntu:16.04
MAINTAINER Anand GAvai <anand.gavai@wur.nl>
RUN apt-get update -y
RUN apt-get install software-properties-common -y
RUN apt-get update -y
RUN apt-get install -y ca-certificates vim
RUN useradd -ms /bin/bash feedcompositionintake

USER feedcompositionintake
WORKDIR /home/feedcompositionintake
COPY data /home/feedcompositionintake/data

CMD while sleep 10m; do echo "Feed Composition Intake container is running"; done
