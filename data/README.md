## Overview

The data on feed composition, feed intake per day and animal weight has been collected for livestock (pigs, poultry and cattle) in the Netherlands.


> At this moment we are not using any controlled terms or ontologies, as we do not have access to them. 
If you come across relevant resources, please get in touch with us.


## Background of this data
The purpose of this data is to make a model for the intake of unwanted substances by livestock via feed.

For every variable the definition is given below.

## Data Dictionary

### Variable definitions

| Variable        | Definition                                | Units Of Measurement if Applicable                           | Reference Ontology | 
|-----------------|-------------------------------------------|--------------------------------------------------------------|--------------------|
| Identifier     | Unique Identifier that corresponds with ProductCategory, ProductSubCategory, AnimalSpecies, AnimalSubSpecies and Year respectively                    | NA                                                           | NA   |   |
| ProductCategory    | Category of raw materials                        | NA                                                           | NA                 |   |
| ProductSubCategory  | Subtypes of raw material within the ProductCategory                       | NA                                                           | NA                 
|AnimalSpecies   | Category of Animal | NA                                      | NA                 |   |
| AnimalSubSpecies|   Subtypes of animal within the AnimalSpecies category                                     | NA                                    | NA                 
| RiskFeedAnimalSubSpecies    | Name of animalsubspecies in the RiskFeedModel corresponding to AnimalSubSpecies| NA                                     | NA                 
| AnimalSubSpeciesWeight     | Weight of the animal            | Kilograms |            NA        
| AnimalSubSpeciesConsumption     | Amount of Feed eaten by AnimalSubSpecies per day              |Kilograms |     NA             
| AnimalSubSpeciesCompoundFeedIntake | Percentage of RawMaterialSubCategory in the composition of the diet of the AnimalSubspecies              | Percentage relative to the total diet                             |NA                   
| Year            | Year when data was recorded        | NA                                                           |       NA             |  |
| MinAnimalSubSpeciesCompoundFeedIntake        |The minimum percentage from the AnimalSubSpeciesCompoundFeedIntake of all years                                         |   NA                                                           | NA                  
| MaxAnimalSubSpeciesCompoundFeedIntake     |The maximum percentage from the AnimalSubSpeciesCompoundFeedIntake of all years                                          |       NA                                                       |NA                    
 
 Below, the variables and their definitions are explained.
 
### Identifier
The unique productcode consists of 5 numbers, each corresponding with a variable described below.

| Position 	| Number 	| RawMaterial        	| Position 	| Number 	| RawMaterialSubcategory                  	|Reference |
|----------	|--------	|-------------------	|----------	|--------	|---------------------------------------	|----------|
| First    	| 1      	| Grains            	| Second   	| 1      	| Wheat                                 	|RiskFeed  |
|          	|        	|                   	|          	| 2      	| Barley                                	|
|          	|        	|                   	|          	| 3      	| Maize                                 	|
|          	|        	|                   	|          	| 4      	| Triticale                             	|
|          	|        	|                   	|          	| 5      	| Rye                                   	|
|          	|        	|                   	|          	| 6      	| Sorghum                               	|
|          	|        	|                   	|          	| 7      	| Oats                                  	|
|          	|        	|                   	|          	| 8      	| Buckwheat                             	|
|          	|        	|                   	|          	| 9      	| CCM                                   	|
|          	|        	|                   	|          	| 10     	| Other cereals                         	|
|          	| 2      	| GrainByproduct    	|          	| 1      	| Wheat products                        	|
|          	|        	|                   	|          	| 2      	| Maize products                        	|
|          	|        	|                   	|          	| 3      	| Rice by-products                      	|
|          	|        	|                   	|          	| 4      	| By-products other cereal grains       	|
|          	|        	|                   	|          	| 5      	| Bakery products                       	|
|          	|        	|                   	|          	| 6      	| DDGS                                  	|
|          	| 3      	| OilseedByproduct  	|          	| 1      	| Soya expeller/extracted               	|
|          	|        	|                   	|          	| 2      	| Sunflowerseed expeller/extracted      	|
|          	|        	|                   	|          	| 3      	| Coconut/copra expeller/extracted      	|
|          	|        	|                   	|          	| 4      	| Palmkernel expeller/extracted         	|
|          	|        	|                   	|          	| 5      	| Rapeseed expeller/extracted           	|
|          	|        	|                   	|          	| 6      	| Maize germ meal                       	|
|          	|        	|                   	|          	| 7      	| Maize gluten feed                     	|
|          	|        	|                   	|          	| 8      	| Linseed expeller/extracted            	|
|          	|        	|                   	|          	| 9      	| Groundnut expeller/extracted          	|
|          	|        	|                   	|          	| 10     	| Cottonseed expeller/extracted         	|
|          	|        	|                   	|          	| 11     	| Soya hulls                            	|
|          	|        	|                   	|          	| 12     	| Soya concentrate                      	|
|          	|        	|                   	|          	| 13     	| Rumen-protected rapeseed extracted    	|
|          	|        	|                   	|          	| 14     	| Rumen-protected soya extratec         	|
|          	|        	|                   	|          	| 15     	| Maize glutenmeal                      	|
|          	| 4      	| Legumes           	|          	| 1      	| Pea                                   	|
|          	|        	|                   	|          	| 2      	| Lupines                               	|
|          	|        	|                   	|          	| 3      	| Beans                                 	|
|          	| 5      	| Oilseeds          	|          	| 1      	| Soya bean                             	|
|          	|        	|                   	|          	| 2      	| Rapeseed                              	|
|          	|        	|                   	|          	| 3      	| Linseed                               	|
|          	|        	|                   	|          	| 4      	| Sunflowerseeds                        	|
|          	|        	|                   	|          	| 5      	| Miscellaneous seeds                   	|
|          	| 6      	| Diverse           	|          	| 1      	| Algae                                 	|
|          	|        	|                   	|          	| 2      	| Manioc                                	|
|          	|        	|                   	|          	| 3      	| Grass-/clover-/alfalfa meal           	|
|          	|        	|                   	|          	| 4      	| Dried beet pulp                       	|
|          	|        	|                   	|          	| 5      	| Citrus pulp                           	|
|          	|        	|                   	|          	| 6      	| Sugar cane molasses                   	|
|          	|        	|                   	|          	| 7      	| Vinasse                               	|
|          	|        	|                   	|          	| 8      	| Potato protein                        	|
|          	|        	|                   	|          	| 9      	| Protapec                              	|
|          	|        	|                   	|          	| 10     	| Sugar beet                            	|
|          	| 7      	| AnimalProducts    	|          	| 1      	| Weypowder/WPC                         	|
|          	|        	|                   	|          	| 2      	| Milkpowder/-concentrate               	|
|          	|        	|                   	|          	| 3      	| Animal proteins                       	|
|          	|        	|                   	|          	| 4      	| Fish meal                             	|
|          	|        	|                   	|          	| 5      	| Bovine fat                            	|
|          	|        	|                   	|          	| 6      	| Porcine fat                           	|
|          	|        	|                   	|          	| 7      	| Other animal fats                     	|
|          	|        	|                   	|          	| 8      	| Fish oil                              	|
|          	|        	|                   	|          	| 9      	| Poultry fat                           	|
|          	| 8      	| OilsFatsVegetable 	|          	| 1      	| Coconut fat                           	|
|          	|        	|                   	|          	| 2      	| Palmoil                               	|
|          	|        	|                   	|          	| 3      	| Palmkernel fat                        	|
|          	|        	|                   	|          	| 4      	| Rapeseed oil                          	|
|          	|        	|                   	|          	| 5      	| Soya bean oil                         	|
|          	|        	|                   	|          	| 6      	| Sunflower oil                         	|
|          	|        	|                   	|          	| 7      	| Miscellaneous plant incl. linseed oil 	|
|          	|        	|                   	|          	| 8      	| Mixed fats and fatty acids            	|
|          	| 9      	| MinorAdditives    	|          	| 1      	| Lime/limestone                        	|
|          	|        	|                   	|          	| 2      	| Salt                                  	|
|          	|        	|                   	|          	| 3      	| Sodium-bicarbonate                    	|
|          	|        	|                   	|          	| 4      	| Premix                                	|
|          	|        	|                   	|          	| 5      	| Mono-calcium phosphate                	|
|          	|        	|                   	|          	| 6      	| Magnesium oxide                       	|
|          	|        	|                   	|          	| 7      	| L-Lysine HCL                          	|
|          	|        	|                   	|          	| 8      	| DL-Methionine                         	|
|          	|        	|                   	|          	| 9      	| L-Threonine                           	|
|          	|        	|                   	|          	| 10     	| L-Tryptofane                          	|
|          	|        	|                   	|          	| 11     	| L-Valine                              	|
|          	|        	|                   	|          	| 12     	| Fytase mix                            	|
|          	|        	|                   	|          	| 13     	| Organic acid                          	|
|          	|        	|                   	|          	| 14     	| Enzym                                 	|
|          	|        	|                   	|          	| 15     	| L-Arginine                            	|
|          	|        	|                   	|          	| 16     	| Urea                                  	|
|          	|        	|                   	|          	| 17     	| Copper sulphate                       	|
|          	|        	|                   	|          	| 18     	| Clay materials                        	|



| Position 	| Number 	| AnimalSpecies 	| Position 	| Number 	| AnimalSubSpecies     	|Reference|
|----------	|--------	|------------	|----------	|--------	|------------------------	|---------|
| Third    	| 1      	| Pigs       	| Fourth   	| 1      	| Piglet                 	|RiskFeed |
|          	|        	|            	|          	| 2      	| Meat pig               	|
|          	|        	|            	|          	| 3      	| Gilt                   	|
|          	|        	|            	|          	| 4      	| Sow reproduction       	|
|          	| 2      	| Poultry    	|          	| 1      	| Broiler                	|
|          	|        	|            	|          	| 2      	| Laying hen <18wk       	|
|          	|        	|            	|          	| 3      	| Laying hen >18wk       	|
|          	| 3      	| Cattle     	|          	| 1      	| Milk cow; protein poor 	|
|          	|        	|            	|          	| 2      	| Milk cow; protein rich 	|
|          	|        	|            	|          	| 3      	| Young cattle           	|
|          	|        	|            	|          	| 4      	| Beef cattle            	|



| Position 	| Number 	| Definition 	|
|----------	|--------	|------------	|
| Fifth    	| 1      	| 2016       	|
|          	| 2      	| 2015       	|
|          	| 3      	| 2014       	|
|          	| 4      	| 2013       	|

Unfortunately, when the data from 2017 becomes available, this will be number 5. It was too time consuming to change the number order for the years.
From 2017 onwards it will be in the right order again.

### ProductCategory and ProductSubCategory

Raw materials are defined by the RiskFeed data. For a definition: see the EU_1017_2017 or the feed conversion table (still under construction)

| ProductCategory       	| ProductSubCategory                	| Units of Measurements 	| Reference Ontology 	| Reference |
|-------------------	|---------------------------------------	|-----------------------	|--------------------	|-----------|
| Grains            	| Wheat                                 	| NA                    	| NA                 	|RiskFeed   |
|                   	| Barley                                	|                       	|                    	|
|                   	| Maize                                 	|                       	|                    	|
|                   	| Triticale                             	|                       	|                    	|
|                   	| Rye                                   	|                       	|                    	|
|                   	| Sorghum                               	|                       	|                    	|
|                   	| Oats                                  	|                       	|                    	|
|                   	| Buckwheat                             	|                       	|                    	|
|                   	| CCM                                   	|                       	|                    	|
|                   	| Other cereals                         	|                       	|                    	|
| GrainByproduct    	| Wheat products                        	|                       	|                    	|
|                   	| Maize products                        	|                       	|                    	|
|                   	| Rice by-products                      	|                       	|                    	|
|                   	| By-products other cereal grains       	|                       	|                    	|
|                   	| Bakery products                       	|                       	|                    	|
|                   	| DDGS                                  	|                       	|                    	|
| OilseedByproduct  	| Soya expeller/extracted               	|                       	|                    	|
|                   	| Sunflowerseed expeller/extracted      	|                       	|                    	|
|                   	| Coconut/copra expeller/extracted      	|                       	|                    	|
|                   	| Palmkernel expeller/extracted         	|                       	|                    	|
|                   	| Rapeseed expeller/extracted           	|                       	|                    	|
|                   	| Maize germ meal                       	|                       	|                    	|
|                   	| Maize gluten feed                     	|                       	|                    	|
|                   	| Linseed expeller/extracted            	|                       	|                    	|
|                   	| Groundnut expeller/extracted          	|                       	|                    	|
|                   	| Cottonseed expeller/extracted         	|                       	|                    	|
|                   	| Soya hulls                            	|                       	|                    	|
|                   	| Soya concentrate                      	|                       	|                    	|
|                   	| Rumen-protected rapeseed extracted    	|                       	|                    	|
|                   	| Rumen-protected soya extratec         	|                       	|                    	|
|                   	| Maize glutenmeal                      	|                       	|                    	|
| Legumes           	| Pea                                   	|                       	|                    	|
|                   	| Lupines                               	|                       	|                    	|
|                   	| Beans                                 	|                       	|                    	|
| Oilseeds          	| Soya bean                             	|                       	|                    	|
|                   	| Rapeseed                              	|                       	|                    	|
|                   	| Linseed                               	|                       	|                    	|
|                   	| Sunflowerseeds                        	|                       	|                    	|
|                   	| Miscellaneous seeds                   	|                       	|                    	|
| Diverse           	| Algae                                 	|                       	|                    	|
|                   	| Manioc                                	|                       	|                    	|
|                   	| Grass-/clover-/alfalfa meal           	|                       	|                    	|
|                   	| Dried beet pulp                       	|                       	|                    	|
|                   	| Citrus pulp                           	|                       	|                    	|
|                   	| Sugar cane molasses                   	|                       	|                    	|
|                   	| Vinasse                               	|                       	|                    	|
|                   	| Potato protein                        	|                       	|                    	|
|                   	| Protapec                              	|                       	|                    	|
|                   	| Sugar beet                            	|                       	|                    	|
| AnimalProducts    	| Weypowder/WPC                         	|                       	|                    	|
|                   	| Milkpowder/-concentrate               	|                       	|                    	|
|                   	| Animal proteins                       	|                       	|                    	|
|                   	| Fish meal                             	|                       	|                    	|
|                   	| Bovine fat                            	|                       	|                    	|
|                   	| Porcine fat                           	|                       	|                    	|
|                   	| Other animal fats                     	|                       	|                    	|
|                   	| Fish oil                              	|                       	|                    	|
|                   	| Poultry fat                           	|                       	|                    	|
| OilsFatsVegetable 	| Coconut fat                           	|                       	|                    	|
|                   	| Palmoil                               	|                       	|                    	|
|                   	| Palmkernel fat                        	|                       	|                    	|
|                   	| Rapeseed oil                          	|                       	|                    	|
|                   	| Soya bean oil                         	|                       	|                    	|
|                   	| Sunflower oil                         	|                       	|                    	|
|                   	| Miscellaneous plant incl. linseed oil 	|                       	|                    	|
|                   	| Mixed fats and fatty acids            	|                       	|                    	|
| MinorAdditives    	| Lime/limestone                        	|                       	|                    	|
|                   	| Salt                                  	|                       	|                    	|
|                   	| Sodium-bicarbonate                    	|                       	|                    	|
|                   	| Premix                                	|                       	|                    	|
|                   	| Mono-calcium phosphate                	|                       	|                    	|
|                   	| Magnesium oxide                       	|                       	|                    	|
|                   	| L-Lysine HCL                          	|                       	|                    	|
|                   	| DL-Methionine                         	|                       	|                    	|
|                   	| L-Threonine                           	|                       	|                    	|
|                   	| L-Tryptofane                          	|                       	|                    	|
|                   	| L-Valine                              	|                       	|                    	|
|                   	| Fytase mix                            	|                       	|                    	|
|                   	| Organic acid                          	|                       	|                    	|
|                   	| Enzym                                 	|                       	|                    	|
|                   	| L-Arginine                            	|                       	|                    	|
|                   	| Urea                                  	|                       	|                    	|
|                   	| Copper sulphate                       	|                       	|                    	|
|                   	| Clay materials                        	|                       	|                    	|

### AnimalSpecies

| Variable          | Definition 	|   Units of Measurement	|Reference Ontology |Reference|
|------------------	|---------------|---------------------------|-------------------|---------|
| Pigs        	| Domesticated mammal from the family of Suidae                     	|NA 	|NA| Wikipedia|
| Poultry         	|Domesticated bird from the family of Phasianidae        	|NA           	|NA|
| Cattle 	| Domesticated mammal from the family of Bovidae           	|       NA    	|NA|

### AnimalSubSpecies

### Pigs

| Variable | Definition         | Units of Measurement if Applicable | Reference Ontology |Reference|
|----------|--------------------|------------------------------------|--------------------|---------|
|Piglet |Piglets that are grown to become a meat pig| <25 kilograms| NA|RiskFeed
| MeatPigs | Pigs used for meat production | Between 75-115 in Kilograms        | NA                 |
| Gilt         |Reproduction sows before their first litter|      NA                              | NA                   |
|Sow reproduction| Sows after their first litter |NA|NA

### Poultry
| Variable | Definition         | Units of Measurement if Applicable | Reference Ontology |Reference|
|----------|--------------------|------------------------------------|--------------------|---------|
|Broiler |Chicks grown for meat production| NA | NA| RiskFeed|
| Laying hen <18 weeks | Hens held for egg production older than 18 weeks | NA        |NA              |
| Laying hen >18 weeks        | Hens held for egg production older than 18 weeks|   NA                |   NA                 |

### Cattle 
| Variable | Definition         | Units of Measurement if Applicable | Reference Ontology |Reference|
|----------|--------------------|------------------------------------|--------------------|---------|
|Milk cow; protein poor |Cows that produce milk for human consumption and are a compound feed with a low protein content| NA| NA| RiskFeed
|Milk cow; protein rich | Cows that produce milk for human consumption and are a compound feed with a high protein content | NA       | NA                 |
|Young cattle         |Young cows that are grown to become a milk cow|      NA                              | NA                   |
|Beef cattle | Cattle used for meat production |NA|NA


### RiskFeedAnimalSubSpecies

Because the names of the AnimalSubSpecies are in Dutch in RiskFeed, the table below shows the corresponding RiskFeed AnimalSubSpecies name to the AnimalSubSpecies in the database

| AnimalSubSpecies  | RiskFeedSubSpecies        | Units of Measurement if Applicable | Reference Ontology |Reference|
|-----------|-------------------|------------------------------------|--------------------|-------------------------|
|Piglet    | Big <25kg | NA    |NA| RiskFeed
|Meat pig     |Vleesvarken 75-115kg                   | NA                                   | NA                   |
|Gilt| Zeugen opfok|NA|NA
|Sow reproduction| Zeugen reproductie|NA|NA
| Broiler| Vleeskuikens|NA|NA
| Laying hen <18 wk| Leghen <18 wk|NA|NA
|Laying hen >18 wk| Leghen >18 wk|NA|NA
|Milk cow; protein poor| Melkvee eiwit-arm|NA|NA
|Milk cow; protein rich| Melkvee eiwit-rijk|NA|NA
|Young cattle| Jongvee melk| NA|NA
|Beef cattle|Vleesvee afmest|NA |NA

### AnimalSubSpeciesWeight

### Pigs
| AnimalSubspecies           	| Weight 	| Units of Measurement    	| Reference Ontology 	|Reference|
|------------------	|--------	|-----------	|--------------------	|-----------------------|
| Piglet           	| 20     	| Kilograms 	|      NA              	|EFSA|
| Meat pig         	| 100    	| Kilograms 	|        NA            	|
| Gilt             	| 50     	| Kilograms 	|          NA          	|
| Sow reproduction 	| 200    	| Kilograms 	|            NA        	|

### Poultry
| AnimalSubspecies           	| Weight 	| Units of Measurement    	| Reference Ontology 	|Reference|
|------------------	|--------	|-----------	|--------------------	|-----------------------|
| Broiler           	| 2    	| Kilograms 	|      NA              	|  EFSA
| Laying hen <18 weeks         	| 2   	| Kilograms 	|        NA            	|
| Laying hen >18 weeks          	| 2   	| Kilograms 	|          NA          	|

### Cattle
| AnimalSubspecies           	| Weight 	| Units of Measurement    	| Reference Ontology 	|Reference|
|------------------	|--------	|-----------	|--------------------	|-----------------------|
| Milk cow; protein-poor           	| 650     	| Kilograms 	|      NA              	|EFSA
| Milk cow; protein-rich        	| 650    	| Kilograms 	|        NA            	|
| Young cattle    	| NA    	| Kilograms 	|          NA          	|
| Beef cattle 	| 400   	| Kilograms 	|            NA        	|

### AnimalSubSpeciesConsumption

### Pigs
| AnimalSubspecies 	| Consumption 	| Units of Measurement    	| Reference Ontology 	|Reference
|------------------	|--------	|-----------	|--------------------	|---------------|
| Piglet           	| 1     	| Kilograms 	|      NA              	|EFSA
| Meat pig         	| 3    	| Kilograms 	|        NA            	|
| Gilt             	| 2   	| Kilograms 	|          NA          	|
| Sow reproduction 	| 6    	| Kilograms 	|            NA        	|

### Poultry
| AnimalSubspecies           	| Consumption 	| Units of Measurement    	| Reference Ontology 	|Reference
|------------------	|--------	|-----------	|--------------------	|---------------------------|
| Broiler           	| 0.12   	| Kilograms 	|      NA              	|EFSA
| Laying hen <18 weeks         	| 0.12   	| Kilograms 	|        NA            	|
| Laying hen >18 weeks          	| 0.12   	| Kilograms 	|          NA          	|

### Cattle
| AnimalSubspecies           	| Consumption 	| Units of Measurement    	| Reference Ontology 	|Reference
|------------------	|--------	|-----------	|--------------------	|---------------------------|
| Milk cow; protein-poor           	| 8     	| Kilograms 	|      NA              	|EFSA
| Milk co; protein-rich        	| 8    	| Kilograms 	|        NA            	|
| Young cattle    	| NA    	| Kilograms 	|          NA          	|
| Beef cattle 	| 1.5   	| Kilograms 	|            NA        	|

### AnimalSubSpeciesCompundFeedIntake
This variable is sufficiently explained under 'Variable definitions'

### Year
This variable is sufficiently explained under 'Variable definitions'

### MinAnimalSubSpeciesCompundFeedIntake and MaxAnimalSubSpeciesCompundFeedIntake
This variables show the minimum-maximum range over all years included, as explained under 'Variable definitions'


## Variable Notes
None
